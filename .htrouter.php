<?php

// is it a static asset?
if (preg_match('/\.(?:png|jpg|jpeg|gif|css)$/', $_SERVER["REQUEST_URI"])) {
    return false;
}

// php script, so here we go
require_once __DIR__ . '/public/index.php';
