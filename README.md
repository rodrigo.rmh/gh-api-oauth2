# Github v3 API demo
This is a PHP application that generates a report with some stats about Github repositories. It lists the top contributors of some random repo, plus activity information, similar to https://github.com/cc65/cc65/graphs/commit-activity. It uses the Github API v3 (https://developer.github.com/v3/).

This is a "no-framework" solution. It does not meet the PSR standards (PHP Framework Interop Group) -- if you need that you might as well use a framework then... I wanted to push myself and have some fun hacking it. For example, I've implemented the OAuth2 authentication flow from scratch. It also contains a simple routing mechanism (separate endpoints for the authentication flow and some "heavier" page fragments, like the top contributor's list), limited controllers and a view class to render PHP templates.

It demonstrates the following:
- Basic application level routing;
- Oauth2 flow;
- Rudimentary caching for somewhat quicker loading;

For a "real world" application, I'd most likely go for a more established framework that closely follows the PSR. For a (not so) quick hack, I'm satisfied.

## Running
 
1. Clone this repository
2. If you're interested in this you probably already have PHP installed. If not: 

`sudo apt install php php-curl`

3. Create your application on the [Github Developer Settings](https://github.com/settings/developers) page. I've used the following settings:

![image](https://gitlab.com/rodrigo.rmh/gh-api-oauth2/-/raw/master/s2.png)

4. Fire up the builtin PHP webserver

`php -S localhost:3333 -t public .htrouter.php `

(port number because my home router setup was not playing nice with the auth callback.)

This should appear:

![image](https://gitlab.com/rodrigo.rmh/gh-api-oauth2/-/raw/master/s3.png)

Then, should you proceed:

![image](https://gitlab.com/rodrigo.rmh/gh-api-oauth2/-/raw/master/s4.png)

After that, you'll finally be able to browse some repositories - I've provided a few. Every time you click the "random project" link, a new one will be chosen at random.

![image](https://gitlab.com/rodrigo.rmh/gh-api-oauth2/-/raw/master/s5.png)

![image](https://gitlab.com/rodrigo.rmh/gh-api-oauth2/-/raw/master/s6.png)


## Caveats
If on PHP 7.4+, the builtin webserver supports true paralell request serving, and everything should be a wee little bit snappier.

Some API calls are not instant; for example, the weekly commits call will often answer with HTTP 202, expecting you to call it again, after "giving the job a few moments to complete". If you do so, the second call will respond with 200 and the data you expect. I could not determine how long "a few moments" really is, but empirically, some stubborn repos started giving me their data after a minute or so. This is mitigated in the case of popular repositories (makes sense, since those are probably hit on the API frequently as well).

The commits API can track a lot of information (which makes sense, because Github itself is nearing 12 years old). However the activity API only serves data from the last 52 weeks.

The definition of "most active period" gave me pause: is it the week with most commits in the last year (as per above)? Is it the week with the most additions/deletions, no matter the size? I chose the former, but that could be easily changed.

```php
// most active period = most commits in a week?
$score = intval(array_sum($activity[$k]['days']));

// most active period = (lines added + lines removed)?
$score = intval($activity[$k]['added']) + abs(intval($activity[$k]['removed']));
```

## TODO
- Put link to profile on contributor names
- Make links open in new window, including repo link @ the top
- Fix the cached/unreliable activity data, as per the "Caveats" section above
- Implement naive cached expiry using `filemtime`
- Convert from using `file_get_contents` and `file_put_contents` to streams.
