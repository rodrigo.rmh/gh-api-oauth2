<?php
session_start();

require 'src/bootstrap.php';

$routes = require 'config/routes.php';

$r = new Router();
foreach ($routes as $k => $v) {
	$r->route($k, function() use($app, $v){
		require $v;
	});
}

$uri = trim(explode('?', $_SERVER['REQUEST_URI'])[0], '/');

$r->dispatch($uri);
