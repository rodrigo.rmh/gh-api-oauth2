<?php

class View
{

    public static function render($view, $vars = [], $buffer = false){
        ob_start();
        extract($vars);
        include "src/views/" . $view . '.phtml';
        if ($buffer) return ob_get_clean();
        echo ob_get_clean();
    }

}
