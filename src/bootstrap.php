<?php 

$app = new stdClass();
$app->config = require 'config/config.php';

$projects = [
	"pmmp/PocketMine-MP",
	"cc65/cc65",
	"PHPMailer/PHPMailer",
	"laravel/framework",
	"danielmiessler/SecLists",
	"WordPress/WordPress",
	"averne/Turnips",
	"rshipp/tornps",
	"avidity/sentry-onpremise",
	"fabricjs/fabric.js",
	"dciabrin/ngdevkit",
	"Overv/outrun",
	"ssokolow/quicktile",
	"lonekorean/wordpress-export-to-markdown",
	"clvv/fasd",
	"spatie/yaml-front-matter",
	"fogleman/primitive",
	"instantpage/instant.page",
	"tessalt/echo-chamber-js",
	"be5invis/Iosevka",
	"mame/quine-relay",
	"propjockey/css-sweeper",
	"hwayne/awesome-cold-showers",
	"coronalabs/corona",
	"taeber/cwith",
	"vladocar/Basic.css",
	"f-prime/Blunt",
	"arcanericky/ga-cmd",
	"nesdoug/33_MMC3",
	"pinobatch/bnrom-template",
	"djmasde/catwm",
	"jayphelps/git-blame-someone-else",
	"hsoft/collapseos",
	"slembcke/neslib-template",
	"motoharu-gosuto/psvcd",
	"gitter-badger/jQuiti",
];

if (!isset($_SESSION['repo_owner'])){
	shuffle($projects);
	$_SESSION['repo_owner'] = $projects[0]; ;
}

$app->repo_owner = $_SESSION['repo_owner'];

require 'src/Router.php';
require 'src/View.php';
require 'src/Cache.php';
require 'src/GitHubAPI.php';

$app->api = new GitHubAPI();
$app->cache = new Cache($app->config['CACHE_DIR']);

function get_layout(){
	return "layout.phtml";
}