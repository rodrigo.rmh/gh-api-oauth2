<?php

class Cache
{
	private $cacheDir = "";

	public function __construct($cacheDir){
		$this->cacheDir = $cacheDir;
        if (!file_exists($this->cacheDir)) {
            mkdir($this->cacheDir, 0700, true);
        }
	} 

    public function setItem($key, $data){
    	file_put_contents(
            $this->cacheDir . "/" . $key,
            serialize($data)
        );
    }

    public function getItem($key){
    	$path = $this->cacheDir . "/" . $key;
    	if (file_exists($path)) return unserialize(file_get_contents($path));
    	return false;
    }

    public function generateKey($scope, $info){
        // $rawKey = $_SERVER['REMOTE_ADDR'] .serialize($scope) . serialize($info);
        $rawKey = $scope .'_'. md5($info);
		return hash('md5', $rawKey);
    }

}
