<?php
class Router {
	private $routes = [];

	function route($action, Closure $callback)
	{
		$action = trim($action, '/');
		$this->routes[$action] = $callback;
	}

	function dispatch($action)
	{
		$action = trim($action, '/');
		$callback = $this->routes[$action];
		echo call_user_func($callback);
	}
}