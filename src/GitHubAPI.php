<?php

class GitHubAPI
{
    protected $apiBase = "https://api.github.com";
    function __construct(){}

    function getRepo($repo){
        try {
            $resource = $this->apiBase . "/repos/" . $repo;
            return $this->genericApiRequest($resource);
        } catch (Exception $e){
            die($e->getMessage());
        }
    }

    function getContributors($repo){
        try {
            $resource = $this->apiBase . "/repos/" . $repo . "/contributors?q=contributions&order=desc&per_page=3";
            return $this->genericApiRequest($resource);
        } catch (Exception $e){
            die($e->getMessage());
        }
    }

    function getCommits($repo, $user){
        try {
            $resource = $this->apiBase . "/repos/" . $repo . "/commits?per_page=1&author=" . $user;
            return $this->genericApiRequest($resource);
        } catch (Exception $e){
            die($e->getMessage());
        }
    }

    /*
    This route when 1st called returns 202, then you must wait for a while and call it again
     */
    function getWeeklyCodeFrequency($repo){
        try {
            $resource = $this->apiBase . "/repos/" . $repo . "/stats/code_frequency";
            return $this->genericApiRequest(
                $resource,
                true
            );
        } catch (Exception $e){
            die($e->getMessage());
        }
    }

    /*
    Last year only :(
    These don't match 100%, my guess is the "insights" tab inside github.com
    considers anonynmous comments as well, while the API  doesn't.
    
    As per github:
    "Computing repository statistics is an expensive operation, so we try to return cached data whenever possible. If the data hasn't been cached when you query a repository's statistics, you'll receive a 202 response; a background job is also fired to start compiling these statistics. Give the job a few moments to complete, and then submit the request again. If the job has completed, that request will receive a 200 response with the statistics in the response body."

    Repository statistics are cached by the SHA of the repository's default branch; pushing to the default branch resets the statistics cache.
     */
    function getWeeklyCommitActivity($repo){
        //!!!!! https://api.github.com/repos/cc65/cc65/stats/commit_activity !!!!!!
        try {
            $resource = $this->apiBase . "/repos/" . $repo . "/stats/commit_activity";
            return $this->genericApiRequest(
                $resource,
                true
            );
        } catch (Exception $e){
            die($e->getMessage());
        }
    }

    // GNARLY hack to get to the `link:` response header that has HTML
    // containing navigation links. This HTML is pArSeD to return the last page
    // URL for the commits page
    function getLastPageCommits($repo, $user){
        $url = $this->apiBase . "/repos/" . $repo . "/commits?per_page=1&author=" . $user;

        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);

        $headers[] = 'Accept: application/json';
        $headers[] = 'user-agent: php/client';
        $headers[] = 'Authorization: Bearer ' . $_SESSION['access_token'];

        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_HEADER, true);

        $headers = curl_exec($ch);
        curl_close($ch);

        $data = [];
        $headers = explode(PHP_EOL, $headers);

        foreach ($headers as $row) {
            if (strlen($row) > 0 && stripos($row, ':') != -1) {
                $parts = explode(':', $row, 2);
                if (count( $parts) >1){
                    $data[trim($parts[0])] = trim($parts[1]);
                }
            }
        }

        $headers = $data;
        if (isset($headers['link'])){
            $h = $headers['link'];
            $h = preg_replace('/<*>*/i','',$h);
            $h = explode(',', $h)[1];
            $h = explode(';', $h)[0];
            $h = trim($h);
            return $h;
        }
        return "";
    
    }

    function genericApiRequest($url, $asArray=false) {
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);

        $headers[] = 'Accept: application/json';
        $headers[] = 'user-agent: php/client';

        $headers[] = 'Authorization: Bearer ' . $_SESSION['access_token'];

        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        $response = curl_exec($ch);

        return json_decode($response, $asArray);
    }

}
