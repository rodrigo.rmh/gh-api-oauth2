<?php

// generates a random session state.
// will be compared with the value that github sends back
$_SESSION['state'] = hash('sha256', microtime(TRUE).rand().$_SERVER['REMOTE_ADDR']);

$params = [
	'client_id' => $app->config['OAUTH2_CLIENT_ID'],
	// 'redirect_uri' => "http://{$_SERVER['SERVER_NAME']}:{$_SERVER['SERVER_PORT']}/oauth2-callback",
	'redirect_uri' => "http://localhost:3333/oauth2-callback",
	'scope' => 'repo read:user',
	'state' => $_SESSION['state']
];

// redirects to github's auth flow
$loc = $app->config['OAUTH2_AUTH_URL'] . '?' . http_build_query($params);
header("location: {$loc}");