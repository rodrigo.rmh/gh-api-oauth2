<?php
// Fetches data about repo contributors (number of commits, oldest and newest commit...)

if (!isset($_SESSION['access_token']))
{
	die();
}

$contrib_data = $app->cache->getItem(
	$app->cache->generateKey("contributors", $app->repo_owner)
);
if (!$contrib_data) {

	$contributors = $app->api->getContributors($app->repo_owner);

	foreach ($contributors as $k => $v) {
		$c = [
			'user' => $v->login,
			'contributions' => $v->contributions,
			'avatar_url' => $v->avatar_url
		];

		$newestPage = $app->api->getCommits($app->repo_owner, $v->login);

		// fetches the 'Link:' header from the request to obtain the last page
		// in the commits listing endpoint. This SHOULD be combined with the
		// above to save on API requests, but time is running out :/
		$last_page_url = $app->api->getLastPageCommits($app->repo_owner, $v->login);

		$oldestPage = $app->api->genericApiRequest($last_page_url);

		if ($oldestPage !== NULL){
			$oldest = new DateTime($oldestPage[0]->commit->author->date);
			$c['oldest'] = $oldest->format("Y-m-d");
		}

		if (!empty( $newestPage )){
			$newest = new DateTime($newestPage[0]->commit->author->date);
			$c['newest'] = $newest->format("Y-m-d");
		}
		
		$contrib_data[] = $c;
	}

	$app->cache->setItem(
		$app->cache->generateKey("contributors", $app->repo_owner),
		$contrib_data
	);
}

die(View::render('contributors', [
	'contrib_data' => $contrib_data,
]));
