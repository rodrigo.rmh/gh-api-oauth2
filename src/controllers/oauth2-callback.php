<?php
// If Github _somehow_ didn't send the `code` parameter, fail
if (!isset($_GET['code'])){
	header('location: /');
	exit;
}

// Confirms that the value coming from the response matches.
// If state does not match, something is wrong
// In other words: MUST have a state, MUST be the same state that returned
if (!isset($_GET['state']) || $_SESSION['state'] != $_GET['state']) {
	header('location: /');
	exit;
}

$postFields = [
	'client_id'     => $app->config['OAUTH2_CLIENT_ID'],
	'client_secret' => $app->config['OAUTH2_CLIENT_SECRET'],
	'redirect_uri'  => 'http://localhost:3333/oauth2-callback',
	'state'         => $_SESSION['state'],
	'code'          => $_GET['code']
];

$url = $app->config['OAUTH2_TOKEN_URL'];
$ch = curl_init($url);

$headers[] = 'Accept: application/json';

curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($postFields));
curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

$response = curl_exec($ch);
$token = json_decode($response);

$_SESSION['access_token'] = $token->access_token;
header('location: /');
