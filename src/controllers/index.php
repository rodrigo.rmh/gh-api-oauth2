<?php
// Fetches data about the repository itself (logo, name, description...)

if (!isset($_SESSION['access_token']))
{
	die(View::render('guest', []));
}

$repo_owner = $_SESSION['repo_owner'];
$objRepo = $app->cache->getItem(
	$app->cache->generateKey("general", $app->repo_owner)
);

if (!$objRepo) {
	$objRepo = $app->api->getRepo($app->repo_owner);
	$app->cache->setItem(
		$app->cache->generateKey("general", $app->repo_owner),
		$objRepo
	);
}

die(View::render('report', [
	'full_name' => $objRepo->full_name,
	'description' => $objRepo->description,
	'avatar_url' => $objRepo->owner->avatar_url,
	'html_url' => $objRepo->owner->html_url,
]));
