<?php
// Fetches data about repository activity (commit activity, code frequency...)

if (!isset($_SESSION['access_token']))
{
	die();
}

$activity_meta = $app->cache->getItem(
	$app->cache->generateKey("activity_meta", $app->repo_owner)
);

if (!$activity_meta) {

	$activity  = $app->api->getWeeklyCommitActivity($app->repo_owner);
	$frequency = $app->api->getWeeklyCodeFrequency($app->repo_owner);

	// truncates to last year (52 weeks) of frequency data,
	// since the commit_activity API only goes so far
	$frequency = array_slice($frequency, -52);

	// for calculating the most active period
	$busiest = 0;
	$activityScore = 0;

	foreach ($activity as $k => $v) {
		$activity[$k]['date'] = gmdate("Y-m-d", $activity[$k]['week']);

		if (isset($frequency[$k])) {
			$activity[$k]['added']   = $frequency[$k][1];
			$activity[$k]['removed'] = $frequency[$k][2];
		}

		$activity[$k]['commits'] = intval(array_sum($activity[$k]['days']));
		// most active period = (lines added + lines removed)?
		// $score = intval($activity[$k]['added']) + abs(intval($activity[$k]['removed']));

		// most active period = most commits in a week?
		// $score = intval(array_sum($activity[$k]['days']));
		$score = $activity[$k]['commits'];

		if ($score > $activityScore) {
			$activityScore = $score;
			$busiest = $k;
		}
	}

	// if theres in fact activity data... avoids div by zero
	if (count($activity) > 0){ 
		$average_commits_per_week = intval(array_sum(array_map(function($item){
			return $item['commits'];
		}, $activity)) / count($activity));
		
		$average_additions_per_week = intval(array_sum(array_map(function($item){
			return isset($item['added']) ? $item['added']:0;
		}, $activity)) / count($activity));
		
		$average_deletions_per_week = intval(array_sum(array_map(function($item){
			return isset($item['removed']) ? $item['removed']:0;
		}, $activity)) / count($activity));
	}

	$activity_meta = [];
	$activity_meta['most_active_period'] = $activity[$busiest];
	$activity_meta['average_commits_per_week'] = $average_commits_per_week;
	$activity_meta['average_additions_per_week'] = $average_additions_per_week;
	$activity_meta['average_deletions_per_week'] = $average_deletions_per_week;

	$app->cache->setItem(
		$app->cache->generateKey("activity_meta", $app->repo_owner),
		$activity_meta
	);
}

die(View::render('activity-stats', [
	'activity_meta' => $activity_meta
]));
