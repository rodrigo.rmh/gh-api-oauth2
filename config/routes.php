<?php
return [
    '' => 'src/controllers/index.php',
    'index.php' => 'src/controllers/index.php',
    'public/index.php' => 'src/controllers/index.php',

    // app routes
    'auth' => 'src/controllers/auth.php',
    'oauth2-callback' => 'src/controllers/oauth2-callback.php',
    'logout' => 'src/controllers/logout.php',
    'activity' => 'src/controllers/activity.php',
    'contributors' => 'src/controllers/contributors.php',
    'random' => 'src/controllers/random.php',
];